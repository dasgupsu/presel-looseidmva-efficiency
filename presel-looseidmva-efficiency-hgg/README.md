# Presel_LooseIdMVA_Efficiency_Hgg

## Install stable branch :
  #### cmsrel CMSSW_10_6_8
  #### cd CMSSW_10_6_8/src
  #### cmsenv
  #### git clone https://gitlab.cern.ch/dasgupsu/presel-looseidmva-efficiency-hgg.git 
  #### make

\
**Note:** If you modify anything in *histUtils.pyx* then you need to run *python setup.py build_ext --inplace --use-cython*  before **make** in the previous instructions.

**Note:** This package does not have any CMSSW dependenies. However, we are using this package inside the CMSSW release just to ensure that its getting appropriate version of gcc, ROOT, RooFit, etc.

**Note:** This package was modified from EGM official one (https://github.com/cms-egamma/egm_tnp_analysis/tree/master). Additional background function *Bernstein polynomial* and *Chisquare/NDF* calculation for passing and failing probe fit status are added. We change *settings.py* file present in *etc/config* directory accordingly to measure the Preselection and Loose Photon IDMVA efficiencies.


## Quick description:
1) The main python fitter code is: **tnpEGM_fitter.py** 
2) The interface between the user and the fitter is solely done via the settings file **etc/config/settings.py**
3) We have changed this settings file to accomodate our presel and IDMVA cuts in Hgg analysis.\
The latest modofied settings file is : **settings_Hgg_UL_TnP_200423.py**

4) This settings file takes input Data/MC root trees from **etc/inputs/tnpSampleDef.py** \
The latest modofied input file for Hgg is: **tnpSampleDef_Hgg_TnP.py**
5) The differnt functions used for fitting are defined within the directory **libCpp**

## The different fitting steps:
1) Check Eta-R9 bins: **python tnpEGM_fitter.py etc/config/settings_Hgg_UL_TnP_200423.py  --flag passingPresel --checkBins**

2) Create the bining: **python tnpEGM_fitter.py etc/config/settings_Hgg_UL_TnP_200423.py  --flag passingPresel --createBins**

3) Create the histograms: **python tnpEGM_fitter.py etc/config/settings_Hgg_UL_TnP_200423.py  --flag passingPresel --createHists**

4) Nominal fit: **python tnpEGM_fitter.py etc/config/settings_Hgg_UL_TnP_200423.py  --flag passingPresel --doFit**

5) Alternate signal fit: **python tnpEGM_fitter.py etc/config/settings_Hgg_UL_TnP_200423.py  --flag passingPresel --doFit --altSig**

6) Alternate background fit: **python tnpEGM_fitter.py etc/config/settings_Hgg_UL_TnP_200423.py  --flag passingPresel  --doFit  --altBkg**

7)  Ouput txt file: **python tnpEGM_fitter.py etc/config/settings_Hgg_UL_TnP_200423.py  --flag passingPresel  --sumUp**

\
**Note:** Do same thing with the **--flag passingIDMVA** to find *Loose photon IDMVA efficiency*

**Note:** To find systemetric uncertainties due to tag photon PT cut, change addition cut criteria in *setting.py* file. 

**Note:** Redo the failed fiiting bin one with running *python tnpEGM_fitter.py etc/config/settings_Hgg_UL_TnP_200423.py  --flag passingPresel  --doFit  --iBin ib*