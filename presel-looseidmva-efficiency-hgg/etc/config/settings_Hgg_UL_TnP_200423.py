#############################################################
########## General settings
#############################################################

isEB = True
#isEB=False
#isPresel=False
isPresel=True
#flag to be Tested
flags = {
    'passingPresel'   : '(passingPresel == 1)',
    'passingIDMVA'   : '(passingIDMVA == 1)',
    }

if isPresel:
    if isEB:
        baseOutDir = 'SF_UL17ED_021023_HiggsDNA/TnP_SF_passingPresel_EB'
    else:
        baseOutDir = 'SF_UL17ED_021023_HiggsDNA/TnP_SF_passingPresel_EE'
else:
    if isEB:
        baseOutDir = 'SF_UL17ED_021023_HiggsDNA/TnP_SF_passingIDMVA_EB'
    else:
        baseOutDir = 'SF_UL17ED_021023_HiggsDNA/TnP_SF_passingIDMVA_EE'


#############################################################
########## samples definition  - preparing the samples
#############################################################
### samples are defined in etc/inputs/tnpSampleDef_Hgg_TnP.py
### not: you can setup another sampleDef File in inputs

import etc.inputs.tnpSampleDef_Hgg_TnP as tnpSamples
tnpTreeDir = 'PhotonToRECO'

samplesDef = {
    'data'   : tnpSamples.Moriond18_94X['output_data_single'].clone(),
    'mcNom'  : tnpSamples.Moriond18_94X['output_mc_single'].clone(),
}

## can add data sample easily
#samplesDef['data'].add_sample( tnpSamples.ICHEP2016['data_2016_runC_ele'] )
#samplesDef['data'].add_sample( tnpSamples.ICHEP2016['data_2016_runD_ele'] )


## some sample-based cuts... general cuts defined here after
## require mcTruth on MC DY samples and additional cuts
## all the samples MUST have different names (i.e. sample.name must be different for all)
## if you need to use 2 times the same sample, then rename the second one
#samplesDef['data'  ].set_cut('run >= 273726')
#if not samplesDef['mcNom' ] is None: samplesDef['mcNom' ].set_mcTruth()
#if not samplesDef['mcAlt' ] is None: samplesDef['mcAlt' ].set_mcTruth()
#if not samplesDef['tagSel'] is None: samplesDef['tagSel'].set_mcTruth()
#if not samplesDef['tagSel'] is None:
#    samplesDef['tagSel'].rename('mcAltSel_DY_madgraph_ele')
#    samplesDef['tagSel'].set_cut('tag_Ele_pt > 33  && tag_Ele_nonTrigMVA > 0.90')

## set MC weight, simple way (use tree weight) 

weightName = 'totWeight'
if not samplesDef['mcNom' ] is None: samplesDef['mcNom' ].set_weight(weightName)

#if not samplesDef['mcAlt' ] is None: samplesDef['mcAlt' ].set_weight(weightName)
# systematics:-----
#if not samplesDef['tagSel'] is None:
    #samplesDef['tagSel'].set_weight(weightName)
    #samplesDef['tagSel'].rename('mcAltSel_DY_madgraph_ele')                                                  
#    samplesDef['tagSel'].set_cut('tag_Pho_pt > 40')    
###########################################################################
########## bining definition  [can be nD bining]
###########################################################################

#for EB
if isEB:
    biningDef = [
        { 'var' : 'probe_sc_abseta' , 'type': 'float', 'bins': [ 0.0, 1.4442] },
        { 'var' : 'probe_Pho_r9' , 'type': 'float', 'bins': [0.5,0.85,999.0] },
        ]
else:    
    #for EE
    biningDef = [
        { 'var' : 'probe_sc_abseta' , 'type': 'float', 'bins': [ 1.566,2.5] },
        { 'var' : 'probe_Pho_r9' , 'type': 'float', 'bins': [0.8,0.9,999.0] }, 
        ]
    
###################################################################
########## Cuts definition for all samples
###################################################################
### cut
#cutBase   ='mass>60.0 && mass<120.0'
#cutBase = '(passingPresel   == 1)'

if isPresel:
    cutBase = None
    #cutBase  = '(tag_Pho_pt > 43)' #systematics
else:
    cutBase = '(passingPresel   == 1)'

    #cutBase  = '(passingPresel == 1)&&(tag_Pho_pt > 43)'   # for systematics
# can add addtionnal cuts for some bins (first check bin number using tnpEGM --checkBins)
#additionalCuts = { 
#    0 : 'tag_Ele_trigMVA > 0.92 && sqrt( 2*event_met_pfmet*tag_Ele_pt*(1-cos(event_met_pfphi-tag_Ele_phi))) < 45',
#    1 : 'tag_Ele_trigMVA > 0.92 && sqrt( 2*event_met_pfmet*tag_Ele_pt*(1-cos(event_met_pfphi-tag_Ele_phi))) < 45',
#    2 : 'tag_Ele_trigMVA > 0.92 && sqrt( 2*event_met_pfmet*tag_Ele_pt*(1-cos(event_met_pfphi-tag_Ele_phi))) < 45',
#    3 : 'tag_Ele_trigMVA > 0.92 && sqrt( 2*event_met_pfmet*tag_Ele_pt*(1-cos(event_met_pfphi-tag_Ele_phi))) < 45',
#    4 : 'tag_Ele_trigMVA > 0.92 && sqrt( 2*event_met_pfmet*tag_Ele_pt*(1-cos(event_met_pfphi-tag_Ele_phi))) < 45',
#    5 : 'tag_Ele_trigMVA > 0.92 && sqrt( 2*event_met_pfmet*tag_Ele_pt*(1-cos(event_met_pfphi-tag_Ele_phi))) < 45',
#    6 : 'tag_Ele_trigMVA > 0.92 && sqrt( 2*event_met_pfmet*tag_Ele_pt*(1-cos(event_met_pfphi-tag_Ele_phi))) < 45',
#    7 : 'tag_Ele_trigMVA > 0.92 && sqrt( 2*event_met_pfmet*tag_Ele_pt*(1-cos(event_met_pfphi-tag_Ele_phi))) < 45',
#    8 : 'tag_Ele_trigMVA > 0.92 && sqrt( 2*event_met_pfmet*tag_Ele_pt*(1-cos(event_met_pfphi-tag_Ele_phi))) < 45',
#    9 : 'tag_Ele_trigMVA > 0.92 && sqrt( 2*event_met_pfmet*tag_Ele_pt*(1-cos(event_met_pfphi-tag_Ele_phi))) < 45'
#}

#### or remove any additional cut (default)
additionalCuts = None

#additionalCuts = '(tag_Pho_csev < 0.5)'
#additionalCuts = '(tag_Pho_hasPixelSeed > 0.5)'
#additionalCuts = '(tag_Pho_pt > 43)'

#############################################################
########## fitting params to tune fit by hand if necessary
#############################################################
#"meanP[-0.0,-5.0,5.0]","sigmaP[0.5,0.1,5.0]", previous values
#"meanF[-0.0,-5.0,5.0]","sigmaF[0.5,0.1,5.0]", previous values
#    "acmsP[60.,50.,80.]","betaP[0.05,0.01,0.1]","gammaP[0.1, 0, 1]","peakP[90.0,88.0,92.0]",
#    "acmsF[60.,50.,80.]","betaF[0.05,0.01,0.1]","gammaF[0.1, 0, 1]","peakF[90.0, 88., 92.0]",

#changing parameters
 
tnpParNomFit = [
    "meanP[-0.0,-10.0,10.0]","sigmaP[0.19,0.01,25.0]",
    "meanF[-0.0,-8.0,8.0]","sigmaF[0.31,0.01,10.0]",
    "acmsP[60.,50.,80.]","betaP[0.07,0.05,1.3]","gammaP[0.0012, -2.0, 3.0]","peakP[90.0,88.0,92.0]",
    "acmsF[60.,50.,80.]","betaF[0.002,0.001,1.2]","gammaF[0.0017, 0.001, 1.5]","peakF[90.0,88.0,92.0]",
    ]

# original values 
#tnpParNomFit = [
#    "meanP[-0.0,-5.0,5.0]","sigmaP[0.5,0.001,5.0]",
#    "meanF[-0.0,-5.0,5.0]","sigmaF[0.5,0.001,5.0]",
#    "acmsP[60.,50.,80.]","betaP[0.1,0.01,0.5]","gammaP[0.1, -2, 2]","peakP[90.0,88.0,92.0]",
#    "acmsF[60.,50.,80.]","betaF[0.1,0.01,0.5]","gammaF[0.1, -2, 2]","peakF[90.0,88.0,92.0]",
#    ]




#forAltSig
#    "meanP[-0.0,-5.0,5.0]","sigmaP[1,0.7,6.0]","alphaP[2.0,1.2,3.5]" ,'nP[3,-5,5]',"sigmaP_2[1.5,0.5,6.0]","sosP[1,0.5,5.0]",
#    "meanF[-0.0,-5.0,5.0]","sigmaF[2,0.7,15.0]","alphaF[2.0,1.2,3.5]",'nF[3,-5,5]',"sigmaF_2[2.0,0.5,6.0]","sosF[1,0.5,5.0]",
#    "acmsP[60.,50.,75.]","betaP[0.04,0.01,0.06]","gammaP[0.1, 0.005, 1]","peakP[90.0,88.0,92.0]",
#    "acmsF[60.,50.,75.]","betaF[0.04,0.01,0.06]","gammaF[0.1, 0.005, 1]","peakF[90.0,88.0,92.0]",



#changing parameters
#tnpParAltSigFit = [
#    "meanP[-0.0,-5.0,5.0]","sigmaP[5,0.7,15.0]","alphaP[0.2,0.1,6.5]" ,'nP[3,-5,5]',"sigmaP_2[2.1,0.5,6.0]","sosP[1.0,0.1,10.0]",
#    "meanF[-0.0,-5.0,5.0]","sigmaF[2,0.7,15.0]","alphaF[0.2,0.1,4.0]",'nF[3,-5,5]',"sigmaF_2[2.0,0.5,6.0]","sosF[1.0,0.1,10.0]",
#    "acmsP[70.,60.,80.]","betaP[0.1,0.01,0.5]","gammaP[0.1, -2, 2]","peakP[90.0,88.0,92.0]",
#    "acmsF[60.,50.,80.]","betaF[0.1,0.01,0.5]","gammaF[0.1, -2, 2]","peakF[90.0,88.0,92.0]",
#    ]

# Original values

tnpParAltSigFit = [
    "meanP[-0.0,-5.0,5.0]","sigmaP[2.2,0.7,20.0]","alphaP[5.5,0.2,6.5]" ,'nP[3,-5,5]',"sigmaP_2[2.1,0.3,10.0]","sosP[1.0,0.1,50.0]",
    "meanF[-0.0,-5.0,5.0]","sigmaF[2.0,0.7,15.0]","alphaF[2.0,1.2,4.0]",'nF[3,-5,5]',"sigmaF_2[2.0,0.4,10.0]","sosF[1.0,0.1,50.0]",
    "acmsP[60.,50.,80.]","betaP[0.005,0.001,1.0]","gammaP[0.005, 0.001, 1.0]","peakP[90.0,88.0,92.0]",
    "acmsF[60.,50.,80.]","betaF[0.005,0.001,0.5]","gammaF[0.005, 0.001, 1.0]","peakF[90.0,88.0,92.0]",
    ]
     
# Original values
tnpParAltBkgFit = [
    "meanP[-0.0,-10.0,10.0]","sigmaP[0.3,0.1,50.0]",
    "meanF[-0.0,-5.0,5.0]","sigmaF[0.2,0.1,50.0]",
    #"alphaP[0.05,0.01,0.1]",
    #"alphaF[-0.009,-0.009,0.009]",
    "a0P[0.0065,0.0,45.5]","a1P[0.0065,0.0,45.5]","a2P[0.0068,0.0,35.5]","a3P[0.0063,0.0,50.5]",
    "a0F[0.0053,0.0.,30.5]","a1F[0.0053,0.0,30.5]","a2F[0.0063,0.0,30.5]","a3F[0.0063,0.0,30.5]",
    ]
        
