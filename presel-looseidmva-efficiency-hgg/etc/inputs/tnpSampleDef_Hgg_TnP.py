from libPython.tnpClassUtils import tnpSample

# for UL2017 SF Validation uning HiggsDNA and Flashgg 

flashgg_2017ULE_Data ='/eos/user/d/dasgupsu/Run3HggAnalysis/Photon_Preselection/2017UL/Flashgg_Validation/'

flashgg_2017UL_Summer19_MC = '/eos/user/d/dasgupsu/Run3HggAnalysis/Photon_Preselection/2017UL/Flashgg_Validation/'

HiggsDNA_2017ULE_Data = '/eos/user/d/dasgupsu/Run3HggAnalysis/Photon_Preselection/2017UL/HiggsDNA_Validation/'

HiggsDNA_2017UL_Summer19_MC = '/eos/user/d/dasgupsu/Run3HggAnalysis/Photon_Preselection/2017UL/HiggsDNA_Validation/'

Moriond18_94X = {
    'output_mc_single' : tnpSample('output_mc_single', 
                                   HiggsDNA_2017UL_Summer19_MC + 'DYJetsToLL_M-50_TuneCP5_13TeV_2017UL.root',
                                   isMC = True, nEvts =  -1 ),
    'output_data_single': tnpSample('output_data_single' ,
                                    HiggsDNA_2017ULE_Data + 'SingleElectron_UL2017DE.root',
                                    lumi = 36.3e+3 ), 
    }
